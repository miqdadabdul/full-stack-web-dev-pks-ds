<?php

namespace App\Listeners;

use App\Events\RegenerateOtpEvent;
use App\Mail\RegenerateOtpMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmailToUserNewOtp implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RegenerateOtpEvent  $event
     * @return void
     */
    public function handle(RegenerateOtpEvent $event)
    {
        Mail::to($event->user->email)->send(new RegenerateOtpMail($event->user));
    
    }
}
