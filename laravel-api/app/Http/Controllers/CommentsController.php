<?php

namespace App\Http\Controllers;

use App\Comments;
use App\Mail\PostAuthorMail;
use Illuminate\Http\Request;
use App\Mail\CommentAuthorMail;
use App\Events\CommentStoredEvent;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class CommentsController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth:api')->except(['index', 'show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $post_id = $request->post_id;
        $comments = Comments::where('post_id', $post_id)->latest()->get();

        return response()->json([
            'success' => true,
            'message' => 'Data daftar comments berhasil ditampilkan',
            'data' => $comments
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $allRequest = $request->all();
        $validator = Validator::make($allRequest, [
            'content' => 'required',
            'post_id' => 'required',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors() , 400);
        }

        $comments = Comments::create([
            'content' => $request->content,
            'post_id' => $request->post_id,
        ]);

        //memanggil event CommentStoredEvent
        event(new CommentStoredEvent($comments));

        // //ini dikirim kepada yang memiliki post
        // Mail::to($comments->post->user->email)->send(new PostAuthorMail($comments));

        // //ini dikirim kepada yang memiliki comment
        // Mail::to($comments->user->email)->send(new CommentAuthorMail($comments));

        if($comments){
            return response()->json([
                'success' => true,
                'message' => 'Data comment berhasil dibuat',
                'data' => $comments,
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data comment gagal dibuat',
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $comments = Comments::find($id);

        if($comments)
        {
            return response()->json([
                'seccess' =>true,
                'message' =>'Data comment berhasil ditampilkan',
                'data' =>$comments,
            ], 200);
        }
        return response()->json([
            'success' => false,
            'messages' => 'Data dengan id : '.$id.' tidak ditemukan',
        ], 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $allRequest = $request->all();

        $validator = Validator::make($allRequest, [
            'content' => 'required',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors() , 400);
        }

        $comments = Comments::find($id);

        if($comments)
        {
            $user = auth()->user();

            if($comments->user_id != $user->id)
            {
                return response()->json([
                    'success' => false,
                    'messages' => 'data post ini bukan milik user login',
                ], 403);
            }
            $comments->update([
                'content' => $request->content,
            ]);

            return response()->json([
                'success' => true,
                'messages' => 'Data comment berhasil di update',
                'data' => $comments,
            ]);
        }

        return response()->json([
            'success' => false,
            'messages' => 'Data dengan id : '.$id.' tidak ditemukan',
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comments = Comments::find($id);

        if($comments)
        {
            $user = auth()->user();

            if($comments->user_id != $user->id)
            {
                return response()->json([
                    'success' => false,
                    'messages' => 'data comment ini bukan milik user login',
                ], 403);
            }
            $comments->delete();

            return response()->json([
                'seccess' =>true,
                'message' =>'Data commant berhasil di hapus',
                'data' =>$comments,
            ], 200);
        }
        return response()->json([
            'success' => false,
            'messages' => 'Data dengan id : '.$id.' tidak ditemukan',
        ], 404);
    }
}
