<?php

namespace App\Http\Controllers;

use App\Roles;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RolesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $role = Roles::latest()->get();

        return response()->json([
            'success' => true,
            'message' => 'Data daftar Role berhasil ditampilkan',
            'data' => $role
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $allRequest = $request->all();
        $validator = Validator::make($allRequest, [
            'role' => 'required',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors() , 400);
        }

        $role = Roles::create([
            'role' => $request->role,
        ]);

        if($role){
            return response()->json([
                'success' => true,
                'message' => 'Data Role berhasil dibuat',
                'data' => $role,
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data Role gagal dibuat',
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role = Roles::find($id);

        if($role)
        {
            return response()->json([
                'seccess' =>true,
                'message' =>'Data Role berhasil ditampilkan',
                'data' =>$role,
            ], 200);
        }
        return response()->json([
            'success' => false,
            'messages' => 'Data dengan id : '.$id.' tidak ditemukan',
        ], 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $allRequest = $request->all();

        $validator = Validator::make($allRequest, [
            'role' => 'required',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors() , 400);
        }

        $role = Roles::find($id);

        if($role)
        {
            $role->update([
                'role' => $request->role,
            ]);

            return response()->json([
                'success' => true,
                'messages' => 'Data dengan judul : '.$role->role.' berhasil di update',
                'data' => $role,
            ]);
        }

        return response()->json([
            'success' => false,
            'messages' => 'Data dengan id : '.$id.' tidak ditemukan',
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Roles::find($id);

        if($role)
        {
            $role->delete();

            return response()->json([
                'seccess' =>true,
                'message' =>'Data Role berhasil di hapus',
                'data' =>$role,
            ], 200);
        }
        return response()->json([
            'success' => false,
            'messages' => 'Data dengan id : '.$id.' tidak ditemukan',
        ], 404);
    }
}
