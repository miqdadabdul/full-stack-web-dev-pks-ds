<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use Illuminate\Foundation\Console\Presets\React;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth:api')->only(['store', 'update', 'delete']);
    }




    public function index()
    {
        $posts = Post::latest()->get();

        return response()->json([
            'success' => true,
            'message' => 'Data daftar post berhasil ditampilkan',
            'data' => $posts
        ]);
    }


    public function store(Request $request)
    {
        $allRequest = $request->all();
        $validator = Validator::make($allRequest, [
            'title' => 'required',
            'description' => 'required',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors() , 400);
        }

        

        $post = Post::create([
            'title' => $request->title,
            'description' => $request->description,
            
        ]);

        if($post){
            return response()->json([
                'success' => true,
                'message' => 'Data post berhasil dibuat',
                'data' => $post,
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data post gagal dibuat',
        ], 409);

        
    }


    public function show($id)
    {
        $post = Post::find($id);

        if($post)
        {
            return response()->json([
                'seccess' =>true,
                'message' =>'Dtat post berhasil ditampilkan',
                'data' =>$post,
            ], 200);
        }
        return response()->json([
            'success' => false,
            'messages' => 'Data dengan id : '.$id.' tidak ditemukan',
        ], 404);

        
    }



    public function update(Request $request, $id)
    {
        $allRequest = $request->all();

        $validator = Validator::make($allRequest, [
            'title' => 'required',
            'description' => 'required',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors() , 400);
        }

        $post = Post::find($id);

        if($post)
        {
            $user = auth()->user();

            if($post->user_id != $user->id)
            {
                return response()->json([
                    'success' => false,
                    'messages' => 'data post ini bukan milik user login',
                ], 403);
            }

            $post->update([
                'title' => $request->title,
                'description' => $request->description,
            ]);

            return response()->json([
                'success' => true,
                'messages' => 'Data dengan judul : '.$post->title.' berhasil di update',
                'data' => $post,
            ]);
        }

        return response()->json([
            'success' => false,
            'messages' => 'Data dengan id : '.$id.' tidak ditemukan',
        ], 404);
    }


    public function destroy($id)
    {
        $post = Post::find($id);

        if($post)
        {
            $user = auth()->user();

            if($post->user_id != $user->id)
            {
                return response()->json([
                    'success' => false,
                    'messages' => 'data post ini bukan milik user login',
                ], 403);
            }
            $post->delete();

            return response()->json([
                'seccess' =>true,
                'message' =>'Dtat post berhasil di hapus',
                'data' =>$post,
            ], 200);
        }
        return response()->json([
            'success' => false,
            'messages' => 'Data dengan id : '.$id.' tidak ditemukan',
        ], 404);
    }


}
