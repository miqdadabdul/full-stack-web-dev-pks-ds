//soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];

daftarHewan.sort().forEach(function(item){
    console.log(item)
})



// soal 2

function introduce(data){
    
    return "Nama saya " +data.name+ ", umur saya "+data.age+ " tahun, alamat saya di " +data.address+", dan saya punya hobby yaitu " +data.hobby
}

var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
 
var perkenalan = introduce(data)
console.log(perkenalan)



//soal 3

function hitung_huruf_vokal(string){
    var vocal = ["a", "i", "u", "e", "o"]
    var huruf = 0
    for(let i = 0; i < string.length; i++){
        for(let j = 0; j < vocal.length; j++){
            if(string[i].toLowerCase() == vocal[j]){
                huruf++
            }
        }
    } return huruf
}

var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1 , hitung_2) // 3 2




//soal 4

function hitung(angka){
    angkaAwal = -2
    hasil = angkaAwal + angka * 2
    return hasil
}
console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8





