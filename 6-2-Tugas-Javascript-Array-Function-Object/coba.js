var mobil = [
    {merk: "BMW", warna: "merah", tipe: "sedan"}, 
    {merk: "toyota", warna: "hitam", tipe: "box"}, 
    {merk: "audi", warna: "biru", tipe: "sedan"}]

    //buat array baru, isinya object, yang dibuat dari array mobil, tipenya sedan semua
var car = mobil.map(function(item){
    return {
        merk: item.merk,
        warna: item.warna,
        tipe: "sedan"
    }
})

console.log(car)